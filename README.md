# Ustopias: DxArts 200, Fall 2018

![](screenshot.png)

Interact with it [here](https://kayali.io/ustopias).

## Instructions

Run your own copy:
```
$ pip3 install poetry
$ poetry install
$ poetry run ./create-graph.py
$ python3 -m http.server --directory static/
```
Now visit `localhost:8000` in your web browser.

### Using your own texts

If you want to use different texts, create a new `.txt` file under `texts/`.
The add that file to `input_texts` in `create-graph.py`, and rerun the above
steps from `poetry run ./create-graph.py` onwards.

## Acknowledgments
Texts, courtesy [Project Gutenberg](https://www.gutenberg.org):
* *Utopia*, Thomas More
* *Utopia Limited*, Sullivan and Gilbert
* *1984*, George Orwell
* *The Wonderful Wizard of Oz*, Frank Baum

Software Libraries
* [textstat 0.4.1](https://pypi.org/project/textstat/), Shivam Bansal
* [stop-words](https://github.com/Alir3z4/python-stop-words), Alireza Savand
* [vis.js](http://visjs.org), Almende B.V.
