#!/usr/bin/env python3

from itertools import combinations
from collections import Counter
import string
import json

from textstat.textstat import textstat
from stop_words import get_stop_words

TOP_N_WORDS_PER_TEXT = 15

UNINTERESTING_WORDS = get_stop_words('en')

input_texts = [
    {
        'title': 'Utopia, by Thomas More',
        'location': 'texts/utopia.txt'
    },
    {
        'title': 'Utopia Limited',
        'location': 'texts/utopia-limited.txt'
    },
    {
        'title': 'The Wonderful Wizard of Oz',
        'location': 'texts/oz.txt'
    },
    {
        'title': 'Nineteen Eighty-four',
        'location': 'texts/1984.txt'
    }
]

translator = str.maketrans('', '', string.punctuation)

output = open('static/graph.json', 'w')

root = 'Utopias and Dystopias'
book_nodes = [x['title'] for x in input_texts]
word_nodes = set()
edges = [{'from': root, 'to': x} for x in book_nodes]

all_possible_pairs = combinations(input_texts, 2)

for pair in all_possible_pairs:
    first_text = pair[0]
    second_text = pair[1]

    with open(first_text['location'], 'r') as f:
        words_first = Counter(f.read()
            .lower()
            .translate(translator)
            .split())

    with open(second_text['location'], 'r') as f:
        words_second = Counter(f.read()
            .lower()
            .translate(translator)
            .split())

    intersection = (words_first & words_second)

    for word in UNINTERESTING_WORDS:
        if word in intersection:
            del intersection[word]

    for word in intersection.copy():
        if len(word) < 4:
            del intersection[word]

    for word in intersection:
        intersection[word] = intersection[word] * textstat.gunning_fog(word)

    most_common = [x[0] for x in intersection.most_common(TOP_N_WORDS_PER_TEXT)]

    for word in most_common:
        word_nodes = word_nodes | set([word])
        edges.append({
            'from': first_text['title'],
            'to': word
        })
        edges.append({
            'from': second_text['title'],
            'to': word
        })

result = {
    'root': root,
    'book_nodes': book_nodes,
    'word_nodes': list(word_nodes),
    'edges': edges
}

json.dump(result, output)
output.close()
